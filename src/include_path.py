import json
import os


def include_save_file_path() -> dict:
	return_value_dict = dict()
	
	# 設定ファイルのinclude
	open_setting_json = open('settings/setting.json', 'r')
	setting_json = json.load(open_setting_json)
	
	# 保存データ
	return_value_dict['JSON_DATA_DIR'] = setting_json['data_dir']
	return_value_dict['ACCUMULATED_FILE'] = setting_json['accumulated_file']
	return_value_dict['MOST_RECENT_JSON_DATA'] = os.path.join(return_value_dict['JSON_DATA_DIR'], os.path.join(return_value_dict['ACCUMULATED_FILE']), 'data.json')

	# 画像
	return_value_dict['IMAGE_PATH'] = setting_json['image_path']
	return_value_dict['CONVERT_IMG_PATH'] = os.path.join(return_value_dict['JSON_DATA_DIR'], os.path.join(return_value_dict['ACCUMULATED_FILE'], 'image.jpg'))
	return_value_dict['DESKTOP_IMG_PATH'] = setting_json['desktop_img_path']
	return_value_dict['DESKTOP_BASE_IMG'] = setting_json['desktop_base_img']

	# template
	return_value_dict['TEMPLATE_DIR'] = setting_json['template_dir']
	return_value_dict['TEMPLATE_HTML'] = os.path.join(return_value_dict['TEMPLATE_DIR'], setting_json['template']['html'])
	return_value_dict['TEMPLATE_CSS'] = os.path.join(return_value_dict['TEMPLATE_DIR'], setting_json['template']['css'])
	return_value_dict['TEMPLATE_ITEMS'] = os.path.join(return_value_dict['TEMPLATE_DIR'], setting_json['template']['item_names'])

	# output_pdf
	return_value_dict['OUTPUT_PDF_DIR'] = setting_json['output_pdf_dir']
	return_value_dict['OUTPUT_PDF_PATH'] = os.path.join(return_value_dict['OUTPUT_PDF_DIR'], 'output.pdf')
	
	return return_value_dict
