# -*- coding: utf-8 -*-
import glob
import json
import os.path
from PIL import ImageTk, Image
import tkinter as tk
from tkinter import messagebox
import tkinter.simpledialog as simpledialog
import tkinter.ttk as ttk
from functools import partial

import include_path
import save_to_img


# ウィンドウの横幅
FRAME_WIDTH = 700
FRAME_HIGH = 560

ver = 2


class RunGui:
	did_new_file: bool = False
	
	def __init__(self):
		self.main_view = tk.Tk()
		self.main_view.title('Visualization_of_purpose')
		self.main_view.geometry(str(FRAME_WIDTH + int(FRAME_WIDTH * ver/10)) + "x" + str(FRAME_HIGH + int(FRAME_HIGH * ver/10)))
	
		# Styles
		style = ttk.Style()
		style.configure("example.TNotebook.Tab",
		                background="lightyellow",
		                foreground="black")
		
		style.map(
			"example.TNotebook.Tab",
			background={('active', 'orange'),
			            ('disabled', 'black'),
			            ('selected', 'lightgreen')},
			foreground=[('active', 'gray'),
			            ('disabled', 'gray'),
			            ('selected', 'black')])
		
		# 画面切り替え
		nb = ttk.Notebook(self.main_view, style="example.TNotebook")
		
		tab1 = tk.Frame(nb)  # Screen 1
		tab2 = tk.Frame(nb)  # Screen 2
		
		# 画面に追加
		nb.add(tab1, text="目標の設定", padding=10)
		nb.add(tab2, text="過去の目標", padding=10)
		nb.pack(expand=1, fill="both")
		
		# ウィジェットの配置
		PurposeCreation(tab1)
		ViewPDF(tab2)
		
		self.main_view.protocol("WM_DELETE_WINDOW", self.on_closing)
		self.main_view.mainloop()
		
	# 終了時の処理
	def on_closing(self):
		if messagebox.askokcancel("Quit", "Do you want to quit?"):
			self.main_view.destroy()
			self.did_new_file = True
		else:
			self.main_view.destroy()
			self.did_new_file = False


class ViewPDF(tk.Frame):
	def __init__(self, master):
		super().__init__(master)
		self.file_path = include_path.include_save_file_path()
		
		self.left_frame = tk.Frame(self.master)
		self.right_frame = tk.Frame(self.master)
		self.left_frame.pack(side='left', fill=tk.BOTH, expand=True)
		self.right_frame.pack(side='right', fill=tk.BOTH, expand=True)
		
		self.canvas = tk.Canvas(self.right_frame)
		self.canvas.pack(expand=True, fill=tk.BOTH)
		self.listbox = tk.Listbox(self.left_frame, selectmode="single")
		self.create_reload_btn()
		self.create_listbox()
		self.create_canvas()
		self.listbox.select_set(0)
	
	def create_canvas(self):
		img_file_path = os.path.join(os.path.join(self.file_path['JSON_DATA_DIR'], str(self.listbox.get(0))), 'image.jpg')
		image = Image.open(img_file_path)
		img_hight = FRAME_HIGH + int(FRAME_HIGH * ver/10) - 100
		image.thumbnail((10000, img_hight), Image.ANTIALIAS)
		
		# image.thumbnail((int(FRAME_WIDTH + FRAME_WIDTH * ver/10), int(FRAME_HIGH + FRAME_HIGH * ver/10)), Image.ANTIALIAS)
		self.photo_image = ImageTk.PhotoImage(image)
		
		self.update()  # Canvasのサイズを取得するため更新しておく
		canvas_width = self.canvas.winfo_width()
		canvas_height = self.canvas.winfo_height()

		# 画像の描画
		self.canvas.create_image(
			0, 0,
			image=self.photo_image,  # 表示画像データ
			anchor=tk.NW
		)
		
	def create_reload_btn(self):
		btn = tk.Button(self.left_frame, text='reload', command=self._reload_btn_event)
		btn.pack()
	
	def create_listbox(self):
		listbox_value = list(os.path.basename(d) for d in glob.glob(os.path.join(self.file_path['JSON_DATA_DIR'], '*')))
		listbox_value.reverse()
		for lv in listbox_value:
			self.listbox.insert(tk.END, lv)
			
		self.listbox.pack(fill=tk.BOTH, expand=True)
		self.listbox.bind('<<ListboxSelect>>', self._selected)
		
	def _selected(self, event):
		select_item_index = self.listbox.curselection()
		if select_item_index:
			# 入力されたデータを保存
			select_json_path = os.path.join(self.file_path['JSON_DATA_DIR'], os.path.join(self.listbox.get(select_item_index), 'image.jpg'))
			image = Image.open(select_json_path)
			img_hight = (FRAME_HIGH + int(FRAME_HIGH * ver/10) - 100)
			image.thumbnail((10000, img_hight), Image.ANTIALIAS)
			
			# image.thumbnail((int(FRAME_WIDTH + FRAME_WIDTH * ver/10), int(FRAME_HIGH + FRAME_HIGH * ver/10)), Image.ANTIALIAS)
			self.photo_image = ImageTk.PhotoImage(image)
			
			self.update()  # Canvasのサイズを取得するため更新しておく
			canvas_width = self.canvas.winfo_width()
			canvas_height = self.canvas.winfo_height()
			
			# 画像の描画
			self.canvas.create_image(
				0,0,
				image=self.photo_image,  # 表示画像データ
				anchor=tk.NW
			)
		
	def _reload_btn_event(self):
		listbox_value_before = [self.listbox.get(i) for i in range(self.listbox.size())]
		listbox_value_after = [os.path.basename(i) for i in glob.glob(os.path.join(self.file_path['JSON_DATA_DIR'], '*'))]
		
		add_listbox_values = set(listbox_value_before) ^ set(listbox_value_after)
		if add_listbox_values:
			for add_listbox_value_item in add_listbox_values:
				try:
					symmetric_difference_set_index = listbox_value_before.index(add_listbox_value_item)
					self.listbox.delete(symmetric_difference_set_index)
				except ValueError:
					self.listbox.insert(0, add_listbox_value_item)
				

class PurposeCreation(tk.Frame):
	def __init__(self, master):
		super().__init__(master)
		# 設定ファイル読み込み
		self.file_path = include_path.include_save_file_path()
		
		# 項目名のテンプレート読み込み
		json_open = open(self.file_path['TEMPLATE_ITEMS'], 'r')
		self.temporarily_json = json.load(json_open)
		
		# 選択中の項目
		first_key = list(self.temporarily_json.keys())[1]
		self.choosing = first_key
		
		# create widget
		self.left_frame = tk.Frame(self.master)
		self.right_frame = tk.Frame(self.master)
		self.left_frame.pack(side='left', expand=True, fill=tk.BOTH)
		self.right_frame.pack(side='right', expand=True, fill=tk.BOTH)
		
		# Base objectの作成
		self.create_base_obj()
		# Text areaの作成
		self.create_text_area(first_key)
		self.listbox.select_set(0)
	
	def create_base_obj(self):
		# listboxの親フレーム
		left_frame_label = tk.Frame(self.left_frame)
		left_frame_label.pack(side='top')
		
		# listboxのoption
		h = int(FRAME_HIGH/25 + (FRAME_HIGH/25)*(ver/10))
		options_title = dict(bg="white", bd=0, width=20, height=h, fg='black',
		                     listvariable=tk.StringVar(value=list(self.temporarily_json.keys())), selectmode="single")
		# 各項目を表示するlistbox
		self.listbox = tk.Listbox(left_frame_label, options_title)
		self.listbox.pack(side='left', expand=True, fill=tk.BOTH)
		# listboxのclick event
		self.listbox.bind('<<ListboxSelect>>', self._selected)
		
		# listboxのscrollbar
		scrollbar = tk.Scrollbar(left_frame_label, orient=tk.VERTICAL, command=self.listbox.yview)
		self.listbox['yscrollcommand'] = scrollbar.set
		scrollbar.pack(side='left')
		
		# add btn
		f = tk.Frame(self.left_frame)
		f.pack(side='bottom')
		self.add = tk.Button(f, text='+', command=lambda: self._add_listbox())
		self.delete = tk.Button(f, text='-', command=lambda: self._delete_selected_listbox())
		self.add.pack(side='left')
		self.delete.pack(side='left')
	
	def create_text_area(self, label):
		self.textarea_frame = tk.Frame(self.right_frame)
		self.textarea_frame.pack(side='top', expand=True, fill=tk.BOTH)
		
		# 各項目の詳細を入力するtext area
		self.textarea = tk.Text(self.textarea_frame)
		self.textarea.insert('1.0', self.temporarily_json[label])
		self.textarea.pack(side='top', expand=True, fill=tk.BOTH)
		
		# save btn
		btn = tk.Button(self.textarea_frame, text='save', command=partial(self._save_data))
		btn.pack(side='top')
		
		# 選択中の項目を変数に格納
		if self.listbox.curselection():
			self.choosing = self.listbox.get(self.listbox.curselection())
	
	def _add_listbox(self):
		inputdata = simpledialog.askstring("Input Box", "値を入力してください", )
		self.listbox.insert(tk.END, inputdata)
		self.temporarily_json[inputdata] = ''
	
	def _delete_selected_listbox(self):
		# delete json key
		selectedIndex = tk.ACTIVE
		self.listbox.delete(selectedIndex)
		self.temporarily_json.pop(self.choosing)
	
	def _selected(self, event):
		select_item_index = self.listbox.curselection()
		if select_item_index:
			# 入力されたデータを保存
			self.temporarily_json[self.choosing] = self.textarea.get('1.0', 'end -1c')
			# 選択された項目のTextAreaを再構築
			self.textarea_frame.destroy()
			self.create_text_area(self.listbox.get(select_item_index))
			
	def _save_data(self):
		# 入力されたデータを保存
		self.temporarily_json[self.choosing] = self.textarea.get('1.0', 'end -1c')
		print(json.dumps(self.temporarily_json, ensure_ascii=False, indent=2))
		json_data = dict()
		json_data['Title'] = self.temporarily_json['Title']
		json_data['Do_num'] = '01'
		json_data['Contents'] = [{"content_title": k, "content_value": v} for k, v in self.temporarily_json.items() if k != 'Title']
		
		# 辞書オブジェクトをJSONファイルへ出力
		if not os.path.exists(os.path.dirname(self.file_path['MOST_RECENT_JSON_DATA'])):
			os.makedirs(os.path.dirname(self.file_path['MOST_RECENT_JSON_DATA']))
			
		with open(self.file_path['MOST_RECENT_JSON_DATA'], mode='w', encoding='utf-8') as file:
			json.dump(json_data, file, ensure_ascii=False, indent=2)
		
		# 画像として保存
		save_to_img.SaveImage().conversion_image()
