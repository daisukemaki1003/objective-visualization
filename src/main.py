# -*- coding: utf-8 -*-
from datetime import datetime
import json
import os
import sys
import subprocess

import include_path
import gui
import save_to_img
import change_desktop_img


while True:
	if os.path.basename(os.getcwd()) == 'Visualization_of_purpose':
		print('Moved directory')
		print('The current directory is {0}'.format(os.getcwd()))
		break
	elif os.getcwd() == '':
		print('The file structure is not correct')
		sys.exit()
	else:
		print('cd ..')
		os.chdir('..')


def set_up():
	# import settings
	setting_json_path = 'settings/setting.json'
	json_open = open(setting_json_path, 'r')
	json_data = json.load(json_open)
	
	# Save the data name to save in the json
	accumulated_file = str(datetime.now().strftime('%Y%m%d%H%M%S'))
	json_data["accumulated_file"] = accumulated_file
	
	# save desktop image path to json
	desktop_img_path = 'image/' + accumulated_file + '.jpg'
	json_data["desktop_img_path"] = desktop_img_path
	
	# save json
	with open(setting_json_path, mode='w', encoding='utf-8') as file:
		json.dump(json_data, file, ensure_ascii=False, indent=2)


# -------> main loop
set_up()
file_path = include_path.include_save_file_path()

# Display gui for data entry
if gui.RunGui().did_new_file:

	# # Save input data as desktop image
	save_to_img.SaveImage().create_desktop_image()

	if os.path.exists(file_path['DESKTOP_IMG_PATH']):

		# Apply as desktop image
		change_desktop_img.change_wallpaper_mac_wallpaper(desktop_img_path=file_path['DESKTOP_IMG_PATH'])
		command = ['/usr/local/bin/wallpaper', 'get']
		path = subprocess.run(command)
		
		# Delete desktop image
		if os.path.exists(file_path['DESKTOP_IMG_PATH']) and path == file_path['DESKTOP_IMG_PATH']:
			print('remove desktop image')
			os.remove(file_path['DESKTOP_IMG_PATH'])

	else:
		print('The file to set on the desktop does not exist')
else:
	print('The operation on the gui did not finish properly')

