# -*- coding: utf-8 -*-
import glob

import cv2
from datetime import datetime
from jinja2 import Environment, FileSystemLoader
import json
import os
import pdfkit
from pdf2image import convert_from_path
import sys

import include_path

wkhtmltopdf = '/usr/local/bin/wkhtmltopdf'
poppler = '/usr/local/Cellar/poppler/21.11.0/bin'


# エラーをログに出力
def error_print(error_text):
    with open('error.log', 'a') as f:
        f.write(error_text)


# エラーの取得とメイン関数の実行
def error_output(func):
    def _wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as e:
            date = str(datetime.now().strftime('%Y/%m/%d %H:%M'))
            error_text = date + '  ' + func.__name__ + ': ' + str(e) + '\n'
            error_print(error_text)
            print(error_text)
            sys.exit()
    return _wrapper
    

class SaveImage:
    def __init__(self):
        # 設定ファイル読み込み
        self.file_path = include_path.include_save_file_path()
        
    def create_desktop_image(self):
        self.concat_jpg()
        
    def conversion_image(self):
        json_data = self.read_json_data(self.file_path['MOST_RECENT_JSON_DATA'])
        self.conversion_html_to_pdf(json_data)
        self.conversion_pdf_to_jpg()

    def read_json_data(self, json_path):
        json_data_path = os.path.join(json_path)
        json_open = open(json_data_path)
        json_data = json.load(json_open)
    
        # 改行コードを改行タグに変換
        contents_list = []
        for content in json_data['Contents']:
            c_item = dict()
            c_item['content_title'] = str(content['content_title']).replace('\n', '</p><p>')
            c_item['content_value'] = str(content['content_value']).replace('\n', '</p><p>')
            contents_list.append(c_item)
        json_data['Contents'] = contents_list
        
        num_data = len(glob.glob(os.path.join(self.file_path['JSON_DATA_DIR'], '*')))
        if num_data < 10:
            num_data = '0' + str(num_data)
        json_data['NumData'] = num_data
    
        return json_data
    
    @error_output
    def conversion_html_to_pdf(self, select_json_data):
        env = Environment(loader=FileSystemLoader(self.file_path['TEMPLATE_DIR']))
        template = env.get_template(os.path.basename(self.file_path['TEMPLATE_HTML']))
        temporary_save_html = os.path.join(self.file_path['OUTPUT_PDF_DIR'], '__tmp.html')
        
        with open(temporary_save_html, 'wt', encoding='utf-8') as fp:
            fp.write(template.render(select_json_data))
        
        conf = pdfkit.configuration(wkhtmltopdf=wkhtmltopdf)
        options = {'page-size': 'A4', 'encoding': "UTF-8"}
        pdfkit.from_file(temporary_save_html, self.file_path['OUTPUT_PDF_PATH'], css=os.path.join(self.file_path['TEMPLATE_DIR'], os.path.basename(self.file_path['TEMPLATE_CSS'])), options=options, configuration=conf)
        print("ok")
    
    @error_output
    def conversion_pdf_to_jpg(self, fmt='jpeg'):
        back_img = cv2.imread(self.file_path['DESKTOP_BASE_IMG'])
        convert_image = convert_from_path(self.file_path['OUTPUT_PDF_PATH'], size=back_img.shape[0] - 300, poppler_path=poppler)
        convert_image[0].save(self.file_path['CONVERT_IMG_PATH'], fmt)
    
    @error_output
    def concat_jpg(self):
        fore_img = cv2.imread(self.file_path['CONVERT_IMG_PATH'])
        back_img = cv2.imread(self.file_path['DESKTOP_BASE_IMG'])
    
        dx = 100  # 横方向の移動距離
        dy = 150  # 縦方向の移動距離
        h, w = fore_img.shape[:2]
    
        back_img[dy:dy + h, dx:dx + w] = fore_img
        cv2.imwrite(self.file_path['DESKTOP_IMG_PATH'], back_img)






